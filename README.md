# Sobre Lucas De Lavra Pinto


### Redes Sociais
<div>
<a href="https://www.instagram.com/lucasdellavro/" target="_blank">
    <img src="https://img.shields.io/badge/Instagram-E4405F?style=for-the-badge&logo=instagram&logoColor=white">
</a>
<a href="https://www.linkedin.com/in/lucasdelavrapinto/" target="_blank">
    <img src="https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white">
</a>    
</div>


### Conhecimentos e Experiências
<div>
    <img src="https://img.shields.io/badge/PHP-777BB4?style=for-the-badge&logo=php&logoColor=white">
    <img src="https://img.shields.io/badge/Python-3776AB?style=for-the-badge&logo=python&logoColor=white">
</div>
</div>
    <img src="https://img.shields.io/badge/HTML5-E34F26?style=for-the-badge&logo=html5&logoColor=white">
    <img src="https://img.shields.io/badge/CSS-239120?&style=for-the-badge&logo=css3&logoColor=white">
    <img src="https://img.shields.io/badge/JavaScript-323330?style=for-the-badge&logo=javascript&logoColor=F7DF1E">
    <img src="https://img.shields.io/badge/jQuery-0769AD?style=for-the-badge&logo=jquery&logoColor=white">
    <img src="https://img.shields.io/badge/Bootstrap-563D7C?style=for-the-badge&logo=bootstrap&logoColor=white">
</div>
<div>
    <img src="https://img.shields.io/badge/Flask-000000?style=for-the-badge&logo=flask&logoColor=white">
    <img src="https://img.shields.io/badge/Laravel-FF2D20?style=for-the-badge&logo=laravel&logoColor=white">
</div>
<div>
    <img src="https://img.shields.io/badge/MySQL-00000F?style=for-the-badge&logo=mysql&logoColor=white">
    <img src="https://img.shields.io/badge/PostgreSQL-316192?style=for-the-badge&logo=postgresql&logoColor=white">
</div>
<div>
    <img src="https://img.shields.io/badge/Dart-0175C2?style=for-the-badge&logo=dart&logoColor=white">
    <img src="https://img.shields.io/badge/Flutter-02569B?style=for-the-badge&logo=flutter&logoColor=white">
</div>
<div>
    <img src="https://img.shields.io/badge/GitLab-330F63?style=for-the-badge&logo=gitlab&logoColor=white">
    <img src="https://img.shields.io/badge/GitHub-100000?style=for-the-badge&logo=github&logoColor=white">
</div>

## Resumo profissional
>Profissional com mais de 3 anos de experiência como desenvolvedor de sistemas web e aplicativos. \
Amplo conhecimento no desenvolvimento de sites em Laravel (PHP) e front-end (HTML, CSS, JAVASCRIPT, JQuery).  \
Desenvolvedor de aplicativos para Android e iOS com Flutter/Dart.

## Realizações recentes

- 5 Aplicativos publicados na PlayStore (Android) \
confira em https://play.google.com/store/apps/dev?id=4900286718033998179

- 1 Aplicativo publicado na Apple Store (iOS) \
confira em https://apps.apple.com/br/app/autoposto-tio-nico/id1555279055

- 1 Sistema completo rodando, totalmente desenvolvido por mim \
Criei o servidor, banco de dados, API, sistema web e aplicativo.

## Soft Skills 
- Resolução de Problemas Complexos
- Pensamento Crítico
- Analítico
- Resiliente
- Ético

## Formação Acadêmica
- Ampli - Grupo Cogna Educação ( ampli.com.br ) \
Início 10/2021 - Previsão de conclusão 10/2023 \
Análise e Desenvolvimento de Sistemas



## Cursos

- Desenvolvimento Web – PHP & Laravel presencial 2019
- Banco de Dados: MongoDB 2020
- Udemy: Django 2, Python Beginner, Git e Github, Mobile Flutter 2020
- UpInside Treinamentos - Laravel Developer 2021

## Outros Cursos
- Inglês Básico – Fisk Idiomas 2018

## Últimos empregos:

### AgroGalaxy (https://agrogalaxy.com.br/)
```
Desde 01 de Fevereiro de 2022 até o momento.
Função exercida: Desenvolvedor mobile Ionic e Desenvolvedor fullstack php
Linguagem: PHP (Laravel), Banco: PostgreSQL, Mobile: Ionic
```

### UNETVALE (https://unetvale.com.br)
```
Desde 16 de Setembro de 2019 a 17 de Fevereiro de 2022.
Função exercida: Desenvolvedor FullStack PHP e Desenvolvedor mobile Flutter
Linguagem: PHP (Laravel), Banco: PostgreSQL, Mobile: Flutter e Dart
```

### ORGANIZEE (https://www.organizee.com.br)
```
Desde 01 de Julho de 2018 a 15 de Fevereiro de 2019
Função exercida: Desenvolvedor web back-end
Linguagem: PHP (Laravel), Banco: MySQL
```

### BOREAL TECNOLOGIA (Fábrica de Softwares)
```
Desde 26 de Março de 2018 a 15 de Dezembro de 2018
Função exercida: Desenvolvedor web, criação de marketplaces e sistemas.
Linguagem: Python (Flask e Django), Banco: MongoDB
PHP (Laravel),  Banco: MySQL
```

### ORGANISYS SOFTWARE LTDA (BLING www.bling.com.br ) 
```
Desde 22 de Abril de 2015 a 21 de Março de 2018
Função exercida: Suporte Técnico, atendimento Nível 1, Acesso remoto.
```
